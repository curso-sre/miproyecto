FROM ubuntu:18.04

ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get update && apt-get install curl gnupg -y
RUN  apt-get install -y \
    apache2 \
    libapache2-mod-php \
    php-gd \
    php-json \
    php-mysql \
    php-sqlite3 \
    php-curl \
    php-intl \
    php-imagick \
    php-zip \
    php-xml \
    php-mbstring \
    php-soap \
    php-ldap \
    php-apcu \
    php-redis \
    php-dev \
    libsmbclient-dev \
    php-gmp \
    smbclient
RUN echo 'deb http://download.opensuse.org/repositories/isv:/ownCloud:/server:/10/Ubuntu_18.04/ /' | tee /etc/apt/sources.list.d/isv:ownCloud:server:10.list \
    && curl -fsSL https://download.opensuse.org/repositories/isv:ownCloud:server:10/Ubuntu_18.04/Release.key | gpg --dearmor |  tee /etc/apt/trusted.gpg.d/isv_ownCloud_server_10.gpg > /dev/null \
    && apt-get update && apt-get install owncloud-complete-files -y
